﻿using System.Collections.Generic;
using WordChain.Models;

namespace WordChain.Services
{
    public interface IDictionaryFileProcessor
    {
        IList<FourCharacterWord> GetFourCharacterWords(string dictionaryFilePath);
    }
}
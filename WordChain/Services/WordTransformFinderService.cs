﻿using WordChain.Models;
using WordChain.Renderers;

namespace WordChain.Services
{
    public class WordTransformFinderService
    {
        private readonly IDictionaryFileProcessor _dictionaryFileProcessor;
        private readonly string _pathToDictionaryFile;
        private readonly IWordFransformerFactory _wordFransformerFactory;
        private readonly IWordTransformRenderer _wordTransformRenderer;

        public WordTransformFinderService(IDictionaryFileProcessor dictionaryFileProcessor, string pathToDictionaryFile, IWordFransformerFactory wordFransformerFactory, IWordTransformRenderer wordTransformRenderer)
        {
            _dictionaryFileProcessor = dictionaryFileProcessor;
            _pathToDictionaryFile = pathToDictionaryFile;
            _wordFransformerFactory = wordFransformerFactory;
            _wordTransformRenderer = wordTransformRenderer;
        }

        public void BuildTransform(FourCharacterWord startWord, FourCharacterWord endWord)
        {
            var fourCharacterWords = _dictionaryFileProcessor.GetFourCharacterWords(_pathToDictionaryFile);
            
            var wordTransformer = _wordFransformerFactory.Create(startWord, endWord, fourCharacterWords);

            _wordTransformRenderer.Render(wordTransformer.Transform());
        }
    }
}
﻿using System.Collections.Generic;
using WordChain.Models;

namespace WordChain.Services
{
    public class BinaryWordTreeNode
    {
        public BinaryWordTreeNode(FourCharacterWord data)
        {
            Data = data;
        }

        public BinaryWordTreeNode Left { get; set; }

        public BinaryWordTreeNode Right { get; set; }

        public FourCharacterWord Data { get; set; }
    }


    public class WordTreeSearch
    {
        private readonly Queue<BinaryWordTreeNode> _searchQueue;
        private readonly BinaryWordTreeNode _root;

        public WordTreeSearch(BinaryWordTreeNode rootNode)
        {
            _searchQueue = new Queue<BinaryWordTreeNode>();
            _root = rootNode;
        }

        public bool Search(FourCharacterWord data)
        {
            var _current = _root;
            _searchQueue.Enqueue(_root);

            while (_searchQueue.Count != 0)
            {
                _current = _searchQueue.Dequeue();
                if (_current.Data == data)
                {
                    return true;
                }
                else
                {
                    _searchQueue.Enqueue(_current.Left);
                    _searchQueue.Enqueue(_current.Right);
                }
            }

            return false;
        }
    }
}

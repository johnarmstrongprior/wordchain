using System.Collections.Generic;
using WordChain.Models;

namespace WordChain.Services
{
    public class WordFransformerFactory : IWordFransformerFactory
    {
        public IWordTransformer Create(FourCharacterWord startWord, FourCharacterWord endWord, IList<FourCharacterWord> listOfWords)
        {
            return new WordTransformer(startWord, endWord, listOfWords);
        }
    }
}
﻿using System.Collections.Generic;
using System.IO;
using WordChain.Models;

namespace WordChain.Services
{
    public class DictionaryFileProcessor : IDictionaryFileProcessor
    {
        public IList<FourCharacterWord> GetFourCharacterWords(string dictionaryFilePath)
        {
            var wordList = new List<FourCharacterWord>();

            using (var dictionaryFile = File.OpenText(dictionaryFilePath))
            {
                while (!dictionaryFile.EndOfStream)
                {
                    var wordAsString = dictionaryFile.ReadLine();

                    if (wordAsString != null && wordAsString.Trim().Length == 4)
                        wordList.Add(new FourCharacterWord(wordAsString.Trim()));
                }
                dictionaryFile.Close();
            }
            return wordList;
        }
    }
}
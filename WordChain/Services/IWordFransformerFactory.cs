using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using WordChain.Models;

namespace WordChain.Services
{
    public interface IWordFransformerFactory
    {
        IWordTransformer Create(FourCharacterWord startWord, FourCharacterWord endWord, IList<FourCharacterWord> listOfWords);
    }
}
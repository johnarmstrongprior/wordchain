﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using WordChain.Exceptions;
using WordChain.Models;
using WordChain.Utilities;

namespace WordChain.Services
{
    public interface IWordTransformer
    {
        FourCharacterWord[] Transform();
    }

    public class WordTransformer : IWordTransformer
    {

        private readonly FourCharacterWord _startWord;
        private readonly FourCharacterWord _endWord;
        private readonly IList<FourCharacterWord> _wordList;

        public WordTransformer(FourCharacterWord startWord, FourCharacterWord endWord, IList<FourCharacterWord> wordList)
        {
            _startWord = startWord;
            _endWord = endWord;
            _wordList = wordList;
        }

        public FourCharacterWord[] Transform()
        {
       
            var transformSteps = new List<FourCharacterWord>();

            if (String.Equals(_endWord, _startWord, StringComparison.CurrentCultureIgnoreCase))
            {
                transformSteps.Add(_startWord);
                return transformSteps.ToArray();
            }

            var currentWord = _startWord;
            
            var orderedWordList = string.Compare(_startWord, _endWord, true) < 0 ? _wordList.OrderBy(word => word.ToString()).ToList() : _wordList.OrderByDescending(word => word.ToString()).ToList();

            var foundWords = new List<FourCharacterWord>();
            var wordHasBeenFound = false;
            var counter = 0;
            do
            {
                foreach (var listWord in orderedWordList)
                {
                    if (foundWords.All(word => string.Compare(word, listWord, StringComparison.CurrentCultureIgnoreCase) != 0))
                    {
                        if (listWord == _startWord)
                        {
                            transformSteps.Add(_startWord);
                            foundWords.Add(listWord);
                            wordHasBeenFound = true;
                        }
                        else if (transformSteps.Any())
                        {
                            if (listWord == _endWord && WordComparison.AreAdjacent(listWord, currentWord))
                            {
                                transformSteps.Add(listWord);
                                return transformSteps.ToArray();
                            }

                            if (WordComparison.AreAdjacent(listWord, currentWord))
                            {
                                transformSteps.Add(listWord);
                                if (WordComparison.AreAdjacent(listWord, _endWord))
                                {
                                    transformSteps.Add(_endWord);
                                    return transformSteps.ToArray();
                                }
                                foundWords.Add(listWord);
                                currentWord = listWord;
                                wordHasBeenFound = true;
                            }
                        }
                    }
                    
                }
                counter++;
            } while (wordHasBeenFound && counter < 1000);

            throw new TransformCannotBeFoundException();
        }
    }
}
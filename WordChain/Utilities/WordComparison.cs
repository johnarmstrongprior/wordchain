﻿using System;
using WordChain.Models;

namespace WordChain.Utilities
{
    public class WordComparison
    {
        public static bool AreAdjacent(FourCharacterWord word1, FourCharacterWord word2)
        {
            var firstWord = word1.ToString().ToLower();
            var secondWord = word2.ToString().ToLower();

            var charDiffCount = 0;
            for (var charIndex = 0; charIndex < 4; charIndex++)
            {
                if (firstWord[charIndex] != secondWord[charIndex])
                    charDiffCount++;
                if (charDiffCount > 1)
                    return false;
            }
            return true;
        }

        public static bool AreTheSame(FourCharacterWord firstWord, FourCharacterWord secondWord)
        {
            return String.Compare(firstWord, secondWord, StringComparison.CurrentCultureIgnoreCase) == 0;
        }
    }
}

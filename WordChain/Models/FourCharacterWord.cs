﻿using System;

namespace WordChain.Models
{
    public class FourCharacterWord
    {
        private readonly string _word;

        public FourCharacterWord(string word)
        {
            var trimmedWord = word.Trim();
            if (trimmedWord.Length != 4)
                throw new ArgumentException("Word must be excatly for characters long.");
            _word = trimmedWord;        
        }

        public override string ToString()
        {
            return _word;
        }

        protected bool Equals(FourCharacterWord other)
        {
            return string.Equals(_word, other._word, StringComparison.CurrentCultureIgnoreCase);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((FourCharacterWord) obj);
        }

        public static implicit operator string(FourCharacterWord fourCharacterWord)
        {
            return fourCharacterWord.ToString();
        }
        public static implicit operator FourCharacterWord(string stringWord)
        {
            return new FourCharacterWord(stringWord);
        }
        public override int GetHashCode()
        {
            return (_word != null ? _word.GetHashCode() : 0);
        }

        public static bool operator ==(FourCharacterWord left, FourCharacterWord right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(FourCharacterWord left, FourCharacterWord right)
        {
            return !Equals(left, right);
        }
    }
}
using System.IO;
using WordChain.Models;

namespace WordChain.Renderers
{
    public class TextFileWordTransformRenderer : IWordTransformRenderer
    {
        private readonly string _outputFilePath;

        public TextFileWordTransformRenderer(string outputFilePath)
        {
            _outputFilePath = outputFilePath;
        }

        public void Render(FourCharacterWord[] wordTransformList)
        {
            using (var outputFile = File.CreateText(_outputFilePath))
            {
                foreach (var word in wordTransformList)
                {
                    outputFile.WriteLine(word);
                }    
                outputFile.Close();
            }
        }
    }
}
using WordChain.Models;

namespace WordChain.Renderers
{
    public interface IWordTransformRenderer
    {
        void Render(FourCharacterWord[] wordTransformList);
    }
}
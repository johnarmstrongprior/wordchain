using System;
using WordChain.Models;

namespace WordChain.Renderers
{
    public class ConsoleWordTransformRenderer : IWordTransformRenderer
    {
        public void Render(FourCharacterWord[] wordTransformList)
        {
            foreach (var fourCharacterWord in wordTransformList)
            {
                Console.WriteLine(fourCharacterWord);
            }
        }
    }
}
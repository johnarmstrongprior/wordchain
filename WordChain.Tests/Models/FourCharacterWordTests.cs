﻿using System;
using NUnit.Framework;
using WordChain.Models;

namespace WordChain.Tests.Models
{
    [TestFixture]
    public class FourCharacterWordTests
    {
        [TestCase("ghsy", "ghsy")]
        [TestCase("SSYU", "SSYU")]
        [TestCase("Posu", "Posu")]
        [TestCase("Posu ", "Posu")]
        [TestCase(" Posu", "Posu")]
        public void GivenAValidStringWithFourCharactersThenAFourLetterWordCanBeCOntructed(string stringWordSupplied, string expectedValue)
        {
            Assert.That(new FourCharacterWord(stringWordSupplied).ToString(), Is.EqualTo(expectedValue));
        }

        [TestCase("")]
        [TestCase("sss")]
        [TestCase("oaisi")]
        [TestCase(" sss")]
        [TestCase("sss ")]
        public void GivenAValidStringWithMoreOrLessThanCharactersThenAnArgumentExceptionIsThrown(string invalidStringWord)
        {
            Exception exceptionThrown = null;
            try
            {
                new FourCharacterWord(invalidStringWord);
            }
            catch (Exception exception)
            {

                exceptionThrown = exception;
            }
            Assert.That(exceptionThrown, Is.InstanceOf<ArgumentException>());
        }
    }
}

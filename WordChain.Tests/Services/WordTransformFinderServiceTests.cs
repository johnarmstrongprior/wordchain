﻿using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using WordChain.Models;
using WordChain.Renderers;
using WordChain.Services;

namespace WordChain.Tests.Services
{
    [TestFixture]
    public class WordTransformFinderServiceBuildTransformTests
    {
        private Mock<IDictionaryFileProcessor> _mockDictionaryFileProcessor;
        private string _pathToDictionaryFile;
        private Mock<IWordFransformerFactory> _mockWordTransformerFactory;
        private FourCharacterWord _startWord;
        private FourCharacterWord _endWord;
        private List<FourCharacterWord> _fourCharacterWords;
        private Mock<IWordTransformer> _mockWordTransformer;
        private Mock<IWordTransformRenderer> _mockTransformRenderer;
        private FourCharacterWord[] _transformedWordList;

        [SetUp]
        public void WhenFindTransform()
        {
            _startWord = new FourCharacterWord("abcd");
            _endWord = new FourCharacterWord("ccvf");
            _fourCharacterWords = new List<FourCharacterWord>();
            _pathToDictionaryFile = "C:\\path\\to\\dictionary\\file.txt";
            _transformedWordList = new FourCharacterWord[] {};

            _mockDictionaryFileProcessor = new Mock<IDictionaryFileProcessor>();
            _mockDictionaryFileProcessor
                .Setup(processor => processor.GetFourCharacterWords(It.IsAny<string>()))
                .Returns(_fourCharacterWords);

            _mockWordTransformer = new Mock<IWordTransformer>();
            _mockWordTransformer
                .Setup(transformer => transformer.Transform())
                .Returns(_transformedWordList);

            _mockWordTransformerFactory = new Mock<IWordFransformerFactory>();
            _mockWordTransformerFactory
                .Setup(factory => factory.Create(It.IsAny<FourCharacterWord>(), It.IsAny<FourCharacterWord>(), It.IsAny<IList<FourCharacterWord>>()))
                .Returns(_mockWordTransformer.Object);

            _mockTransformRenderer = new Mock<IWordTransformRenderer>();


            new WordTransformFinderService(_mockDictionaryFileProcessor.Object, _pathToDictionaryFile, _mockWordTransformerFactory.Object, _mockTransformRenderer.Object).BuildTransform(_startWord, _endWord);
        }

        [Test]
        public void ThenAllTheFourLetterWordsAreReadFromTheDictionary()
        {
            _mockDictionaryFileProcessor.Verify(processor => processor.GetFourCharacterWords(_pathToDictionaryFile));
        }

        [Test]
        public void ThenAWordTransformerIsCreatedForTheStartANdEndWirdsUsingTheListOfFourCharacterWords()
        {
            _mockWordTransformerFactory.Verify(factory => factory.Create(_startWord, _endWord, _fourCharacterWords));
        }

        [Test]
        public void ThenTheWordTransformIsCarriedOut()
        {
            _mockWordTransformer.Verify(transformer => transformer.Transform());
        }

        [Test]
        public void ThenTheResultingListOfTransformWordsIsRendered()
        {
            _mockTransformRenderer.Verify(renderer => renderer.Render(_transformedWordList));
        }
    }
}

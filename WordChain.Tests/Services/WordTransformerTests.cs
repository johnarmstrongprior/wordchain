﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using WordChain.Models;
using WordChain.Services;

namespace WordChain.Tests.Services
{
    [TestFixture("Spat", "Spat", new[] { "Spat" })]
    [TestFixture("Spin", "Spot", new[] { "Spin", "Spit", "Spot" })]
    [TestFixture("Spat", "Spit", new[] { "Spat", "Spit" })]
    [TestFixture("Span", "Spot", new[] { "Span", "Spat", "Spot" })]
    [TestFixture("Spin", "Spat", new[] { "Spin", "Span", "Spat" })]
    [TestFixture("Spit", "Span", new[] { "Spit", "Spin", "Span" })]
    [TestFixture("spat", "Spit", new[] { "spat", "Spit" })]
    [TestFixture("Spin", "spot", new[] { "Spin", "Spit", "spot" })]
    public class WordTransformerTests
    {
        private readonly FourCharacterWord _startWord;
        private readonly FourCharacterWord _endWord;
        private readonly FourCharacterWord[] _expectedTransformResult;
        private FourCharacterWord[] _transformResult;

        public WordTransformerTests(string startWord, string endWord, string[] expectedTransformResult)
        {
            _startWord = new FourCharacterWord(startWord);
            _endWord = new FourCharacterWord(endWord);
            _expectedTransformResult = expectedTransformResult.Select(word => new FourCharacterWord(word)).ToArray();
        }

        [SetUp]
        public void WhenTransformed()
        {
            var wordTransformer = new WordTransformer(new FourCharacterWord(_startWord), new FourCharacterWord(_endWord), new List<FourCharacterWord> { "Spin", "Spit", "Spat", "Spot", "Span" });

            _transformResult = wordTransformer.Transform();
        }

        [Test]
        public void ThenTheResultingListOfTransitionalWordsIsValid()
        {
            Assert.That(_transformResult, Is.EqualTo(_expectedTransformResult));
        }
    }

    public class WordTransformerAlternateWordListTests
    {
        private FourCharacterWord[] _expectedTransformResult;
        private FourCharacterWord[] _transformResult;

        [SetUp]
        public void WhenTransformed()
        {
            _expectedTransformResult = new FourCharacterWord[] {"AAAA", "ABAA", "ABZA", "ABZZ", "AAZZ"};
            var wordTransformer = new WordTransformer("AAAA", "AAZZ", new List<FourCharacterWord> {"AAAA", "AAZZ", "ABAA", "ABZZ", "ABZA"});

            _transformResult = wordTransformer.Transform();
        }

        [Test]
        public void ThenTheResultingListOfTransitionalWordsIsValid()
        {
            Assert.That(_transformResult, Is.EqualTo(_expectedTransformResult));
        }
    }
}

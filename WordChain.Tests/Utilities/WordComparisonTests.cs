﻿using NUnit.Framework;
using WordChain.Utilities;

namespace WordChain.Tests.Utilities
{
    [TestFixture]
    public class WordComparisonTests
    {
        [TestCase("mole", "Hole")]
        [TestCase("Pill", "Poll")]
        public void AreAdjacentTrueTests(string word1, string word2)
        {
            Assert.That(WordComparison.AreAdjacent(word1, word2), Is.True);
        }

        [TestCase("cccc", "Hole")]
        [TestCase("Pill", "kkkk")]
        public void AreAdjacentFalseTests(string word1, string word2)
        {
            Assert.That(WordComparison.AreAdjacent(word1, word2), Is.False);
        }
    }
}
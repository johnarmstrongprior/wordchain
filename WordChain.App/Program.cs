﻿using System;
using WordChain.Exceptions;
using WordChain.Models;
using WordChain.Renderers;
using WordChain.Services;

namespace WordChain.App
{
    class Program
    {
        static void Main(string[] args)
        {


            var dictionaryFilePath = args[0];
            var startWord = args[1];
            var endWord = args[2];
            var outputFilePath = args[3];

            Console.WriteLine("Attempting to find a transform from '{0}' to '{1}'...", startWord, endWord);
            
            var wordTransFormFinder = new WordTransformFinderService(new DictionaryFileProcessor(), dictionaryFilePath, new WordFransformerFactory(), new TextFileWordTransformRenderer(outputFilePath));
            try
            {
                wordTransFormFinder.BuildTransform(new FourCharacterWord(startWord), new FourCharacterWord(endWord));
                Console.WriteLine("Results written for transform from '{0}' to '{1}' written to file {2}.", startWord, endWord, outputFilePath);
            }
            catch (TransformCannotBeFoundException)
            {
                Console.WriteLine("Could not find a transform for these words.");
            }

            Console.WriteLine("Press enter when finished...");
            Console.ReadLine();
        }
    }
}
